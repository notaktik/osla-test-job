import React, { Component } from 'react';
import PropTypes from 'prop-types';

import MyMessage from './components/Message';
import { Grid } from './components/bootstrap';
import FileLoader from './components/FileLoader';
import Table from './components/Table';

import DataRepository from './utils/DataRepository'

import './App.css';

class App extends Component {
    constructor() {
        super();

        let data = DataRepository.get('data') || {};
        let sort = DataRepository.get('sort') || {};

        let {headers, objects: passengers} = data;

        this.state = {
            message: '',
            level: 'info',
            headers: headers || [],
            passengers: passengers || [],
            sort,
            pages: 1,
            pageSize: 3
        }
    }

    getChildContext() {
        return ({
            sayGood: this.say.bind(this, 'info'),
            sayBad: this.say.bind(this, 'error')
        })
    }

    say(level, message) {
        this.setState({level, message})
    }

    setData(data) {
        DataRepository.set('data', data);
        DataRepository.set('sort', {});
        let {headers, objects: passengers} = data;
        this.setState({headers, passengers, sort: {}, pages: 1});
    }

    reset() {
        DataRepository.clear();
        this.setState(
            {headers: [], passengers: [], sort: {}, pages: 1},
            () => this.say('info', 'App reset')
        );
    }

    sortBy(header) {
        let { sortBy, sortAsc } = this.state.sort,
            newSort;

        if (header === sortBy) {
            newSort = ({ sortBy, sortAsc: !sortAsc });
        } else {
            newSort = ({ sortBy: header, sortAsc: true });
        }

        DataRepository.set('sort', newSort);
        this.setState({sort: newSort});
    }

    render() {
        let { passengers, pages, pageSize, sort } = this.state;
        let { sortBy, sortAsc } = sort;

        let rows = [...passengers].sort(
            (prev, next) => {
                if (prev.hasOwnProperty(sortBy) && next.hasOwnProperty(sortBy)) {
                    let prevSort = prev[sortBy],
                        nextSort = next[sortBy];

                    if (prevSort === nextSort) return 0;

                    if (!isNaN(+prevSort) && !isNaN(+nextSort)) {
                        prevSort = +prevSort;
                        nextSort = +nextSort;
                    }

                    if (prevSort < nextSort) {
                        return sortAsc ? -1 : 1;
                    } else {
                        return sortAsc ? 1 : -1;
                    }
                }
                return 0
            }
        ).slice(0, pages * pageSize);

        return (
            <Grid>
                <MyMessage message={this.state.message} level={this.state.level}/>
                <FileLoader
                    reset={this.reset.bind(this)}
                    onDataReceived={this.setData.bind(this)}/>
                <Table
                    name={'List of passengers'}
                    headers={this.state.headers}
                    objects={rows}
                    sort={this.state.sort}
                    setSort={this.sortBy.bind(this)}/>
                {rows.length < passengers.length &&
                <button onClick={() => this.setState({pages: pages + 1})}>show more</button>}
            </Grid>
        )
    }
}

App.childContextTypes = {
    sayGood: PropTypes.func,
    sayBad: PropTypes.func
};

export default App;
