class DataRepository {
    constructor() {
        this._storage = sessionStorage;
        this._prefix = 'X-CSV-';

        this.get = this.get.bind(this);
        this.set = this.set.bind(this);
        this.clear = this.clear.bind(this);
    }

    get(key) {
        try {
            return JSON.parse(this._storage.getItem(this._prefix + key));
        } catch (error) {
            console.error(error)
        }
    }

    set(key, value) {
        this._storage.setItem(this._prefix + key, JSON.stringify(value));
    }

    clear() {
        let items = this.getAll();
        items.map(item => this._storage.setItem(item.key, '{}'));
    }

    getAll() {
        return Object.entries(this._storage)
            .filter(entry => new RegExp(`^${this._prefix}`).test(entry[0]))
            .map(entry => ({key: entry[0], value: entry[1]}));
    }
}

export default new DataRepository();