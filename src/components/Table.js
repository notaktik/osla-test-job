import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Table.css';

class Table extends Component {
    constructor() {
        super();

        this.tableHeader = this.tableHeader.bind(this);
        this.tableRow = this.tableRow.bind(this);
    }

    tableHeader() {
        const { headers, sort, setSort } = this.props;
        const { sortBy, sortAsc } = sort;

        return (
            <tr className={'my-table__header'}>
                {
                    headers.map(
                        header => {
                            let className = 'my-table__header-item';
                            sortBy === header && (className += ` sorted` + (sortAsc ? ' asc' : ' desc'));
                            return (
                                <th
                                    key={header}
                                    className={className}
                                    onClick={() => setSort(header)}>
                                    {header}
                                </th>
                            )
                        }
                    )
                }
            </tr>
        )
    }

    tableRow(object, index) {
        return (
            <tr key={index} className={'my-table__row'}>
                {
                    this.props.headers.map(
                        key => (
                            <td key={key} className='my-table__row-item'>
                                {object.hasOwnProperty(key) ? object[key] : 'X.x'}
                            </td>
                        )
                    )
                }
            </tr>
        );
    }

    render() {
        let { objects: rows } = this.props;

        return (
            <div>
                {this.props.name && <h4>{this.props.name}</h4>}
                <table className={`my-table`}>
                    <thead>
                    {this.tableHeader()}
                    </thead>
                    <tbody>
                    {rows.map(this.tableRow)}
                    </tbody>
                </table>
            </div>
        )
    }
}

Table.propTypes = {
    headers: PropTypes.arrayOf(PropTypes.string).isRequired,
    objects: PropTypes.arrayOf(PropTypes.object).isRequired,
    name: PropTypes.string,
    sort: PropTypes.shape({
        sortBy: PropTypes.string,
        sortAsc: PropTypes.bool
    }).isRequired,
    setSort: PropTypes.func.isRequired
};

export default Table;