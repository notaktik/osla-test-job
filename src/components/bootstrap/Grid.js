import React from 'react';
import PropTypes from 'prop-types';

const Grid = ({
    fluid = false,
    children
}) => {
    let className = 'container';
    fluid && (className += ' fluid');

    return (
        <div className={className}>
            {children}
        </div>
    )
};

Grid.propTypes = {
    fluid: PropTypes.bool
};

export default Grid;