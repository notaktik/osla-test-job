import React from 'react';
import PropTypes from 'prop-types';

const Row = ({
    justifyContent,
    alignItems,
    children
}) => {
    let className = 'row';
    justifyContent && (className += ` justify-content-${justifyContent}`);
    alignItems && (className += ` align-items-${justifyContent}`);

    return (
        <div className={className}>
            {children}
        </div>
    )
};

Row.propTypes = {
    justifyContent: PropTypes.oneOf(['start', 'center', 'end', 'around', 'between']),
    alignItems: PropTypes.oneOf(['start', 'center', 'end', 'stretch'])
};

export default Row;