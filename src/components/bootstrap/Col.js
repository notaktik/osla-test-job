import React from 'react';
import PropTypes from 'prop-types';

const Col = ({
    xs,
    sm,
    md,
    lg,
    xl,
    children
}) => {
    let className = 'col';
    xs && (className += ` col-${xs}`);
    sm && (className += ` col-sm-${sm}`);
    md && (className += ` col-md-${md}`);
    lg && (className += ` col-lg-${lg}`);
    xl && (className += ` col-xl-${xl}`);

    return (
        <div className={className}>
            {children}
        </div>
    )
};

Col.propTypes = {
    xs: PropTypes.number,
    sm: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number,
    xl: PropTypes.number
};

export default Col;