import React from 'react';
import PropTypes from 'prop-types';

import './Message.css';

const MyMessage = ({
    level = 'info',
    message
}) => (
    <div>
        <div style={{textAlign: 'center'}}>last message from app:</div>
        <div className={`my-message my-message-${level}`}>
            {message}
        </div>
    </div>
);

MyMessage.propTypes = {
    level: PropTypes.oneOf(['info', 'error']),
    message: PropTypes.string.isRequired
};

export default MyMessage;