import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Row, Col } from './bootstrap';

import DataRepository from '../utils/DataRepository';

import './FileLoader.css';

export const MyFileInput = ({
    containerClass = 'my-input-container',
    inputClass = 'my-input',
    ...other
}) => (
    <div className={containerClass}>
        <input
            {...other}
            className={inputClass}
            type={`file`}
            name={`csv`}/>
    </div>
);

MyFileInput.propTypes = {
    containerClass: PropTypes.string,
    inputClass: PropTypes.string,
    onChange: PropTypes.func.isRequired
};

export const MyLoadFileButton = ({
    containerClass = 'my-button-container',
    buttonClass = 'my-button',
    ...other
}) => (
    <div className={containerClass}>
        <button {...other} className={'btn btn-primary ' + buttonClass}>load file</button>
    </div>
);

export const MyResetButton = ({
    containerClass = 'my-button-container',
    buttonClass = 'my-button',
    ...other
}) => (
    <div className={containerClass}>
        <button {...other} className={'btn btn-secondary ' + buttonClass}>reset</button>
    </div>
);

MyResetButton.propTypes = {
    containerClass: PropTypes.string,
    inputClass: PropTypes.string,
    onClick: PropTypes.func.isRequired
};

class FileLoader extends Component {
    constructor() {
        super();
        this.state = {
            file: null
        }
    }

    setFile(e) {
        this.setState({file: e.target.files[0]});
    }

    parseFile() {
        let { sayGood, sayBad } = this.context;

        const reader = new FileReader();

        reader.onload = e => {
            const { result: file } = e.target;
            const lines = file.split('\n');

            if (lines.length === 0) {
                console.error('Too few lines');
            }

            const headers = lines.shift().split(';');

            const validate = array => array.length === headers.length;
            const createObject = array => {
                let object = {};
                array.forEach((item, index) => {
                    object[headers[index]] = item;
                });
                return object;
            };

            let objects = lines.map((line, index) => {
                let split = line.split(';');
                if (!validate(split)) {
                    sayBad(`Oh, your CSV file has strange data on line ${index + 1}`);
                    throw new Error(`Corrupted CSV file on line ${index + 1}`)
                }
                return createObject(split);
            });

            sayGood(`We have successfully parsed your file`);
            DataRepository.clear();

            this.props.onDataReceived({headers, objects});
        };

        reader.onerror = (e) => {
            let { error } = e.target;
            sayBad(`Something went wrong while reading file. That's what we ended up with: ${error.message}`);
            throw new Error(error.name);
        };

        if ( !(this.state.file instanceof Blob) ) {
            sayBad('File is not chosen');
            return;
        }

        try {
            reader.readAsText(this.state.file);
        } catch (error) {
            sayBad(`Something went wrong while reading file. That's what we ended up with: ${error.message}`);
        }
    }

    render() {
        return (
            <Row justifyContent={'around'}>
                <Col xs={12} sm={10} md={6} lg={6} xl={4}>
                    <MyFileInput onChange={this.setFile.bind(this)}/>
                </Col>
                <Col xs={6} sm={5} md={3} lg={3} xl={2}>
                    <MyLoadFileButton onClick={this.parseFile.bind(this)}/>
                </Col>
                <Col xs={6} sm={5} md={3} lg={3} xl={2}>
                    <MyResetButton onClick={this.props.reset}/>
                </Col>
            </Row>
        )
    }
}

FileLoader.propTypes = {
    onDataReceived: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired
};

FileLoader.contextTypes = {
    sayBad: PropTypes.func,
    sayGood: PropTypes.func
};

export default FileLoader;